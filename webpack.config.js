const path = require('path');

module.exports = {
    entry:'./src/App.jsx',
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'public')
    },
    //watch: true, //������ ������� ������������� ������ �� ����������� ������ � ����������� ������
    devServer: {
        contentBase: "./public",
        port:3000
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: 'babel-loader'
            },
            {
                test: /\.scss?$/,
                use: ['style-loader', 'css-loader', 'sass-loader']
            }
        ]
    },
    devtool: 'eval-source-map', //����� ������ ������ � ���������� ����� �����
    resolve: {
        extensions:['json','.js','.jsx', '*'] //����� �� ����� ������������� ����� ������� jsx, json, js(����� ��������������� �� ���������)
    }
};