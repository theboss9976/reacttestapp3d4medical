import React from 'react';

import Button from './Button';

import StyledProfileItem from './styled/StyledProfileItem';
import StyledProfileItemImg from './styled/StyledProfileItemImg';
import StyledProfileItemInfo from './styled/StyledProfileItemInfo';
import StyledProfileItemBtns from './styled/StyledProfileItemBtns';
import StyledProfileItemEditForm from './styled/StyledProfileItemEditForm';
import StyledProfileItemEditFormImg from './styled/StyledProfileItemEditFormImg';
import StyledProfileItemEditFormImgMsg from './styled/StyledProfileItemEditFormImgMsg';
import StyledInput from './styled/StyledInput';


class Profile extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            editing: false
        };

        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if (this.state.editing) {
            let name = this.refs.name;

            name.focus();
            name.setSelectionRange(name.value.length, name.value.length);
        }
    }

    componentDidMount() {
        if (this.props.id === this.props.idFirstAdd) {
            this.setState({editing: true});
        }
    }

    handleCancel() {
        this.setState({editing: false});

        //Если отменяем впервые созданный элемент
        if (this.props.idFirstAdd === this.props.id) {
            this.props.onDelete(this.props.id);
        }
    }

    handleSubmit(event) {
        event.preventDefault();
        let name = this.refs.name.value;
        let birthday = this.refs.birthday.value;

        this.props.onEdit(this.props.id, name, birthday);
        this.setState({editing: false});
    }

    renderDisplay() {
        return (
            <StyledProfileItem>
                <StyledProfileItemImg src={this.props.img} alt={this.props.name}/>
                <StyledProfileItemInfo>{this.props.name}</StyledProfileItemInfo>
                <StyledProfileItemInfo>{this.props.birthday}</StyledProfileItemInfo>

                <StyledProfileItemBtns>
                    <Button className="edit icon" icon="edit" onClick={() => this.setState({editing: true})}/>
                    <Button className="delete icon" icon="delete" onClick={() => this.props.onDelete(this.props.id)}/>
                </StyledProfileItemBtns>
            </StyledProfileItem>
        );
    }

    renderForm() {
        return (
            <StyledProfileItemEditForm onSubmit={this.handleSubmit}>
                <StyledProfileItemEditFormImg>
                    <StyledProfileItemImg src={this.props.img} alt={this.props.name}/>
                    <StyledProfileItemEditFormImgMsg>Выбрать файл</StyledProfileItemEditFormImgMsg>
                </StyledProfileItemEditFormImg>
                <input type="text" ref="name" defaultValue={this.props.name} />
                <input type="text" ref="birthday" defaultValue={this.props.birthday} />

                <StyledProfileItemBtns>
                    <Button className="save icon" icon="save" type="submit"/>
                    <Button className="cancel icon" icon="cancel" type="button"
                            onClick={this.handleCancel}/>
                </StyledProfileItemBtns>
            </StyledProfileItemEditForm>
        );
    }

    render() {
        return this.state.editing ? this.renderForm() : this.renderDisplay();
    }

}

Profile.propTypes = {
    id: React.PropTypes.number.isRequired,
    img: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
    birthday: React.PropTypes.string.isRequired,
    onDelete: React.PropTypes.func.isRequired,
    onEdit: React.PropTypes.func.isRequired,
    idFirstAdd: React.PropTypes.number.isRequired
};

export default Profile;