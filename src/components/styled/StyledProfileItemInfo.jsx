import React from 'react';
import styled from 'styled-components';

const StyledProfileItemInfo = styled.span`
	margin-right: auto;
	
	@media screen and (max-width: 576px){
		margin-right: 0;
	}
	
`;

export default StyledProfileItemInfo;