import React from 'react';
import styled from 'styled-components';

const StyledInput = styled.input`
	font-family: "Roboto", Arial, Helvetica, sans-serif;
    font-size: 1rem;
    color: #757575;
    padding: .5em;
    border-radius: 2px;
    border: 1px solid lightgray;
    outline: none;
`;

export default StyledInput;