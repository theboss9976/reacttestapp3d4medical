import React from 'react';
import styled from 'styled-components';

const StyledHeaderWrapper = styled.header`
	display: flex;
    align-items: center;
    padding: 0 1rem;
    color: white;
    background-color: #222;
    text-align: center;
    text-transform: uppercase;
	
	h1{
		display: inline-block;
        color: #fff;
        margin: 1rem auto;
	}
`;

export default StyledHeaderWrapper; //чтобы мы могли импортировать этот компонент в другом файле компонента