import React from 'react';
import styled from 'styled-components';

const StyledProfileItemEditFormImgMsg = styled.span`
    display: flex;
    align-items: center;
    justify-content: center;
    
    width: 50%;
    height: 50%;
    
    margin: auto;
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
    
    text-align: center;
	background-color: rgba(255, 255, 255, 0.84);
	color: #000;
	font-size: 18px;
	padding: 3px;
`;

export default StyledProfileItemEditFormImgMsg;