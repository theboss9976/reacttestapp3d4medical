import React from 'react';
import styled from 'styled-components';

const StyledProfileItemBtns = styled.div`
	display: flex;
`;

export default StyledProfileItemBtns;