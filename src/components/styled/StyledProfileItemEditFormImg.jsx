import React from 'react';
import styled from 'styled-components';

const StyledProfileItemEditFormImg = styled.div`
	margin-right: 15px;
	// display: flex;
	// align-items: center;
	// justify-content: center;
	position:relative;

	&:hover {
		cursor: pointer;
	}
`;

export default StyledProfileItemEditFormImg;