import React from 'react';
import styled from 'styled-components';

const StyledProfileItemImg = styled.img`
	max-width: 150px;
	margin-right: 15px;
	
	@media screen and (max-width: 576px){
		margin-bottom: 15px;
	}
`;

export default StyledProfileItemImg;