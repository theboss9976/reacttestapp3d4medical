import React from 'react';
import styled from 'styled-components';

const StyledProfileItem = styled.div`
	display: flex;
    font-size: 1rem;
    border-top: 1px solid rgba(0, 0, 0, .1);
    background-color: #fff;
    transition: all .2s;
    -webkit-user-select: none;
    user-select: none;
    padding: 1em;
    align-items: center;
	
	@media screen and (max-width: 576px) {	
		flex-direction: column;
		text-align: center;
		
		button {
			margin: 10px;
		}
	}
	
`;

export default StyledProfileItem;