import React from 'react';
import styled from 'styled-components';
import StyledButton from './StyledButton';

const StyledButtonIcon = StyledButton.extend`
	border-radius: 50%;
    font-size: 24px;
    height: 32px;
    margin-left: 0;
    margin-right: 0;
    min-width: 32px;
    width: 32px;
    padding: 0;
    overflow: hidden;
    line-height: normal;
`;

export default StyledButtonIcon;