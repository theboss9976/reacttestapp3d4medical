import React from 'react';
import styled from 'styled-components';

const StyledProfileItemEditForm = styled.form`
	display: flex;
    font-size: 1rem;
    padding: .85em;
    border-top: 1px solid rgba(0, 0, 0, .1);
    align-items: center;
	
	input {
        flex: 1;
        margin-right: 15px;
    }

    .save {
        margin-left: .5em;
    }
	
	@media screen and (max-width: 576px) {			
		flex-direction: column;
		text-align: center;

		input {
			margin-right: 0;
			margin-bottom: 10px;
		}

		button {
			margin: 10px;
		}
}
`;

export default StyledProfileItemEditForm;