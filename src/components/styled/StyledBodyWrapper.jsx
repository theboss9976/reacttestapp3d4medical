import React from 'react';
import styled from 'styled-components';

const StyledBodyWrapper = styled.div`
	 background-color: #fafafa;
    color: #757575;
    font-family: "Roboto", Arial, Helvetica, sans-serif;
    margin: 0 15px;
`;

export default StyledBodyWrapper;