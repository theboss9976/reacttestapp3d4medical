﻿import React from 'react';
import Button from './Button';

class Add extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            birthday:''
        };

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    nextId() {
        this._nextId = this._nextId || 4;
        return this._nextId++;
    }

    handleSubmit(event) {
        event.preventDefault();
        let profile = {
            id: this.nextId(),
            img:'https://qs-liquids.kz/images/no-thumb.png',
            name: this.state.name,
            birthday: this.state.birthday
        };

        this.props.onAdd(profile);
    };

    render() {
        return (
            <Button className="button button--center" type="button" onClick={this.handleSubmit}>Добавить</Button>
        )
    };
}

Add.propTypes = {
    onAdd: React.PropTypes.func.isRequired,
};

export default Add;