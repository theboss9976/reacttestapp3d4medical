import React from 'react';

import StyledHeaderWrapper from './styled/StyledHeaderWrapper';

function Header(props) {
    return (
        <StyledHeaderWrapper>
            <h1>{props.title}</h1>
        </StyledHeaderWrapper>
    );
}

Header.propTypes = {
    title: React.PropTypes.string.isRequired
};

export default Header;