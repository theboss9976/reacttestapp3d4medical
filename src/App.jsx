﻿import React from 'react';
import ReactDOM from 'react-dom';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'; //для анимирирования

import './styles.scss';

import Header from './components/Header'; //импортируем наш Header чтобы его использовать
import Profile from './components/Profile';
import listProfiles from './listProfiles'; //импортируем наш profiles чтобы его использовать
import Add from './components/Add'; //импортируем наш Add чтобы его использовать

import StyledMainWrapper from './components/styled/StyledMainWrapper';
import StyledBodyWrapper from './components/styled/StyledBodyWrapper';

class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            profiles: [],
            idFirstAdd: 0
        };

        this.handleDelete = this.handleDelete.bind(this);
        this.handleAdd = this.handleAdd.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
    }

    //Вызывается один раз, только на клиенте (не на сервере), сразу же после того, как происходит инициализация рендеринга
    componentDidMount() {
        this.setState({
            profiles: this.props.initialData
        });
    }

    handleAdd(profile) {

        let profiles = [...this.state.profiles, profile];

        this.setState({
            idFirstAdd: profile.id,
            profiles: profiles
        });
    }

    handleDelete(id) {
        let profiles = this.state.profiles.filter(profile => profile.id !== id);

        this.setState({
            profiles: profiles,
            idFirstAdd: 0
        });
    }

    handleEdit(id, name, birthday) {
        let profiles = this.state.profiles.map(profile => {
            if (profile.id === id) {
                profile.name = name,
                    profile.birthday = birthday
            }

            return profile;
        });

        this.setState({
            profiles: profiles,
            idFirstAdd: 0
        });

    }

    render() {
        return (
            <StyledBodyWrapper>
                <StyledMainWrapper>
                    <Header title={this.props.title} profiles={this.state.profiles}/>

                    <ReactCSSTransitionGroup
                        component="section"
                        transitionName="slide"
                        transitionEnterTimeout={500}
                        transitionLeaveTimeout={500}>
                        {this.state.profiles.map(profile =>
                            <Profile
                                key={profile.id}
                                id={profile.id}
                                img={profile.img}
                                name={profile.name}
                                birthday={profile.birthday}
                                onDelete={this.handleDelete}
                                onEdit={this.handleEdit}
                                idFirstAdd={this.state.idFirstAdd}
                            />)}
                    </ReactCSSTransitionGroup>

                    <Add onAdd={this.handleAdd}/>
                </StyledMainWrapper>
            </StyledBodyWrapper>
        );
    }
}

App.propTypes = {
    title: React.PropTypes.string,
    initialData: React.PropTypes.arrayOf(React.PropTypes.shape({
        id: React.PropTypes.number.isRequired,
        img: React.PropTypes.string.isRequired,
        name: React.PropTypes.string.isRequired,
        birthday: React.PropTypes.string.isRequired
    })).isRequired
};

App.defaultProps = {
    title: 'Профили'
};

ReactDOM.render(<App initialData={listProfiles}/>, document.getElementById('root'));